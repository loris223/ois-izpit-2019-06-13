/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var koordinate = e.latlng;
    var q = "https://api.lavbic.net/kraji/lokacija?lat="+koordinate.lat+"&lng="+koordinate.lng;
    $.getJSON(q, function( data ) {
      var bliznjiK = document.getElementById("bliznjiKraji");
      var inn = "<h3>Bližnji Kraji</h3><br>";
      for(var i = 0; i < 5; i++){
        inn += "<b>"+data[i].postnaStevilka+" </b>";
        inn += data[i].kraj + "<br>";
      }
      bliznjiK.innerHTML = inn;
      //console.log(inn);
      //var call = "https://ois-loris223.c9users.io/vreme/"+data[0].postnaStevilka;
      //console.log(call);
      
      $.getJSON("/vreme/"+data[0].postnaStevilka, function( x){
        var vremeO = document.getElementById("vremeOkolica");
        //console.log(data[0].postnaStevilka);
        vremeO.innerHTML = "<h3>Vreme v okolici kraja<br>"+data[0].kraj+"</h3><br>";
        vremeO.innerHTML += "<img id = 'g' src='"+"/slike/"+x.ikona+"'  style='float:left; margin-right=10px;'>";
        vremeO.innerHTML += "<p style='float:left; margin=10px;'>"+x.opis+"</p>";
        vremeO.innerHTML += "<p> "+x.temperatura+"</p>";
        //console.log(x);
        $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
          postnaStevilka: data[0].postnaStevilka,
          kraj: data[0].kraj,
          vreme: x.opis,
          temperatura: x.temperatura
        }), function(t) {
          console.log(t);
          $("#prikaziZgodovino").html("Prikaži zgodovino "+ t.steviloKrajev +" krajev");
        });
      });
    });

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");

    // Prikaži vremenske podatke najbližjega kraja
    $("#vremeOkolica").html("<p>Vreme v okolici kraja<br>...<br></p>");

    // Shrani podatke najbližjega kraja
    

    // Prikaži seznam 5 najbližjih krajev
    $("#bliznjiKraji").html("<p>Bližnji kraji<br>...<br>...<br>...<br>...<br>...</p>");
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}
