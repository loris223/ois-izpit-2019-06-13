if (!process.env.PORT)
  process.env.PORT = 8080;


// Priprava strežnika
var express = require('express');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));


var vremenske_slike = {
  "oblačno": "iconfinder_05_cloud_smile_cloudy_emoticon_weather_smiley_3375695.png",
  "deževno": "iconfinder_06_rain_cloud_cry_emoticon_weather_smiley_3375694.png",
  "sončno": "iconfinder_07_sun_smile_happy_emoticon_weather_smiley_3375693.png",
  "sneženo": "iconfinder_10_snow_coud_emoticon_weather_smiley_3375690.png",
  "pretežno oblačno": "iconfinder_09_cloudy_sun_happy_emoticon_weather_smiley_3375691.png"
};


var seznam_krajev = [];


// Prikaz zemljevida vremena
streznik.get("/", function (zahteva, odgovor) {
  odgovor.redirect('/zemljevid-vremena');
});


// Storitev, ki vrne vremenske podatke za podano poštno številko
streznik.get("/vreme/:postnaStevilka", function(zahteva, odgovor) {
  var postnaStevilka = parseInt(zahteva.params.postnaStevilka, 10);
  if (postnaStevilka == undefined || isNaN(postnaStevilka)) {
    odgovor.send(404, "Manjka ustrezna poštna številka!");
  } else {
    var opi = "oblačno";
    var ikon = "iconfinder_05_cloud_smile_cloudy_emoticon_weather_smiley_3375695.png";
    var temperatur = temperatur = Math.floor((Math.random() * 26)+5); 
    if(Math.floor(postnaStevilka / 1000) == 1 || Math.floor(postnaStevilka / 1000) == 4 ){
      opi = "deževno";
      ikon = "iconfinder_06_rain_cloud_cry_emoticon_weather_smiley_3375694.png";
      temperatur = Math.floor((Math.random() * 16)+5); 
    }
    else if(Math.floor(postnaStevilka / 1000) == 2 || Math.floor(postnaStevilka / 1000) == 3 || Math.floor(postnaStevilka / 1000) == 9){
      opi = "sončno";
      ikon = "iconfinder_07_sun_smile_happy_emoticon_weather_smiley_3375693.png";
      temperatur = Math.floor((Math.random() * 16)+20); 
    }
    else if(Math.floor(postnaStevilka / 1000) == 5 || Math.floor(postnaStevilka / 1000) == 6){
      opi = "pretežno oblačno";
      ikon = "iconfinder_09_cloudy_sun_happy_emoticon_weather_smiley_3375691.png";
      temperatur = Math.floor((Math.random() * 11)+15); 
    }
    else if(Math.floor(postnaStevilka / 1000) == 8){
      opi = "sneženo";
      ikon = "iconfinder_10_snow_coud_emoticon_weather_smiley_3375690.png";
      temperatur = Math.floor(-(Math.random() * 11)-5); 
    }
    var rezultat = {
      opis: opi,
      ikona: ikon,
      temperatura: temperatur
    };
    odgovor.send(rezultat);
  }
});


// Prikaz zgodovine
streznik.get("/zgodovina", function (zahteva, odgovor) {
  odgovor.render('zgodovina', { myVar : seznam_krajev });
});


// Prikaz strani z zemljevidom
streznik.get("/zemljevid-vremena", function (zahteva, odgovor) {
  odgovor.render('zemljevid-vremena');
});


streznik.get("/zabelezi-zadnji-kraj/:json", function(zahteva, odgovor) {
  var rezultat = zahteva.params.json;
  seznam_krajev.push(JSON.parse(rezultat));
  odgovor.send({steviloKrajev: seznam_krajev.length});
});


streznik.listen(process.env.PORT, function () {
  console.log("Strežnik je pognan!");
});
